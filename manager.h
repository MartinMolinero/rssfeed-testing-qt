#ifndef MANAGER_H
#define MANAGER_H
#include <QObject>
#include <QNetworkAccessManager>
#include <QStandardItemModel>
#include <model.h>

class IMyCustomWindow;

class Manager : public QObject
{
    Q_OBJECT
    QNetworkAccessManager netManager;
    Model model;
    IMyCustomWindow *view;
    void managerFinished(QNetworkReply *netReply);
    void downloadProgress(qint64 bytes, qint64 bytesTotal) const;
public:
    Manager(IMyCustomWindow *_view);
    bool openRSS(const QString &url);
    void openItem(const QModelIndex &index) const;
    QStandardItemModel* getModel();
};

#endif // MANAGER_H
