#include <QApplication>
#include <mycustomwindow.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MyCustomWindow* win = MyCustomWindow::getMyCustomWindow();
    return a.exec();
}
