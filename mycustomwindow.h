#ifndef MYCUSTOMWINDOW_H
#define MYCUSTOMWINDOW_H
#include <QObject>
#include <QSplitter>
#include <QListView>
#include <QProgressBar>
#include <QStatusBar>
#include <QLineEdit>
#include <QPushButton>
#include <QHBoxLayout>
#include <manager.h>

struct IMyCustomWindow {
    virtual void setProgressBar(int percent) = 0;
    virtual void setStatusBMessage(const QString& message) = 0;
};

class MyCustomWindow : public QWidget, IMyCustomWindow
{
    Q_OBJECT
    QPushButton pushButton;
    QStatusBar statusBar;
    QLineEdit lineEdit;
    QProgressBar progressBar;
    Manager manager;
    QListView tv;
    QSplitter *splitter;
    QHBoxLayout *layout;
    MyCustomWindow();
    MyCustomWindow(const MyCustomWindow&) = delete;
    MyCustomWindow& operator=(const MyCustomWindow&) = delete;
public:
    static MyCustomWindow* getMyCustomWindow();
    virtual void setProgressBar(int percent);
    virtual void setStatusBMessage(const QString& message);
};

#endif // MYCUSTOMWINDOW_H
