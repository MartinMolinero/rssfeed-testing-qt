#include <mycustomwindow.h>
#include <QSplitter>
#include <QHBoxLayout>

MyCustomWindow* MyCustomWindow::getMyCustomWindow()
{
    static MyCustomWindow myView;
    return &myView;
}
MyCustomWindow::MyCustomWindow() : pushButton("Open RSS feed"),
                                   manager(this)
{
    this->resize(650, 450);
    layout = new QHBoxLayout(this);
    splitter = new QSplitter(Qt::Vertical);

    layout->addWidget(splitter);
    splitter->addWidget(&progressBar);
    splitter->addWidget(&lineEdit);
    splitter->addWidget(&pushButton);
    splitter->addWidget(&tv);
    splitter->addWidget(&statusBar);
    statusBar.showMessage("Hello!");

    tv.setModel(manager.getModel());
    connect(&pushButton, &QPushButton::clicked, [this] { manager.openRSS(lineEdit.text()); });
    connect(&tv, &QListView::doubleClicked, &manager, &Manager::openItem );
    show();
}

void MyCustomWindow::setStatusBMessage(const QString& message)
{
    statusBar.showMessage(message);
}
void MyCustomWindow::setProgressBar(int percent)
{
    progressBar.setValue(percent);
}
