#include <QtTest>
#include <model.h>
#include <manager.h>
#include <mycustomwindow.h>


struct MockingWindow : IMyCustomWindow {
    int percentBar;
    QString messageBar;
    virtual void setProgressBar(int percent) { percentBar = percent; }
    virtual void setStatusBMessage(const QString& message) { messageBar = message; }
};

class Tests : public QObject
{
    Q_OBJECT

    const QString xmlString = QString("<mytest><item> \
                        <link>MyLink1</link>\
                        <title>MyTitle</title>\
                        <description></description>\
                         </item><item> \
                        <link>MyLink2</link>\
                        <title>MyTitle</title>\
                        <description></description>\
                         </item></mytest>");
private slots:
    void ModelReturnsNotNullModel();
    void ModelReturnsEmptyStringForInvalidIndex();
    void ModelUpdatesCorrectly();
    void ModelReturnsCorrentDataForValidIndex();
    void ManagerDetectsInvalidEmptyURL();
    void ModelDoesNotUpdateWhenDataCorrupt();
    void ManagerDetectsInvalidURL();
};

QTEST_MAIN(Tests)

void Tests::ModelReturnsNotNullModel()
{
    Model model;
    QVERIFY(model.getModel() != NULL);
}
void Tests::ModelReturnsEmptyStringForInvalidIndex()
{
    Model model;
    QModelIndex item;
    auto res = model.getItemLink(item);
    QVERIFY(res == "");
}
void Tests::ModelUpdatesCorrectly()
{
    Model mod;
    QDomDocument doc;
    doc.setContent(xmlString);
    auto model = mod.getModel();
    QVERIFY(mod.updateModel(doc));
    QVERIFY(model->columnCount() == 1);
    QVERIFY(model->rowCount() == 2);
}
void Tests::ModelDoesNotUpdateWhenDataCorrupt()
{
    Model mod;
    QDomDocument doc;
    doc.setContent(xmlString);
    mod.updateModel(doc);
    doc.setContent(QString("</corruptedXML>"));
    auto model = mod.getModel();
    QVERIFY(!mod.updateModel(doc));
    QVERIFY(model->columnCount() == 1);
    QVERIFY(model->rowCount() == 2);
}
void Tests::ModelReturnsCorrentDataForValidIndex()
{
    Model mod;
    QDomDocument doc;
    doc.setContent(xmlString);
    mod.updateModel(doc);
    auto model = mod.getModel();
    auto index = model->index(1, 0);
    QVERIFY(mod.getItemLink(index) == "MyLink2");
}
void Tests::ManagerDetectsInvalidEmptyURL()
{
    MockingWindow view;
    Manager man(&view);
    QVERIFY(!man.openRSS(QString("")));
    QVERIFY(view.messageBar == "Invalid URL");
    QVERIFY(view.percentBar == 0);
}
void Tests::ManagerDetectsInvalidURL()
{
    MockingWindow view;
    Manager man(&view);
    QVERIFY(man.openRSS(QString("lalaFake")));
    QTest::qWait(600);
    QVERIFY(view.messageBar == "Load Failed. Invalid RSS Feed");
}


#include "tst_tests.moc"
