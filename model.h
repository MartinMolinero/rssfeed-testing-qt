#ifndef MODEL_H
#define MODEL_H
#include <QStandardItemModel>
#include <QDomDocument>

class Model
{
    QStandardItemModel model;
public:
    QString getItemLink(const QModelIndex &index) const;
    bool updateModel(const QDomDocument& docElem);
    QStandardItemModel* getModel() { return &model; }
};

#endif // MODEL_H
