QT       += core gui widgets xml network

TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
    mycustomwindow.cpp \
    manager.cpp \
    model.cpp

HEADERS += \
    mycustomwindow.h \
    manager.h \
    model.h

test {
    QT += testlib

    SOURCES += tst_tests.cpp
}
else {
    SOURCES += \
        main.cpp
}
