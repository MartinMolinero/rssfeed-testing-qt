#include <QDebug>
#include <model.h>

QString Model::getItemLink(const QModelIndex &index) const
{
    QString res;
    auto link = model.data(index, Qt::UserRole);
    if(link.isValid()) {
        res = link.toString();
    }
    return res;
}
bool Model::updateModel(const QDomDocument& doc)
{
    QDomElement docElem = doc.documentElement();
    QDomNodeList nodeList = docElem.elementsByTagName("item");
    auto items = nodeList.length();
    if(items > 0) {
        model.clear();
        model.insertColumn(0);
        model.insertRows(0, items);
        for (int i = 0; i < nodeList.length(); ++i)
        {
            QDomNode node = nodeList.item(i);
            QDomElement e = node.toElement();
            QString strTitle =  e.elementsByTagName("title").item(0).firstChild().nodeValue();
            QString strLink = e.elementsByTagName("link").item(0).firstChild().nodeValue();
            QString strDescription = e.elementsByTagName("description").item(0).firstChild().nodeValue();
            QString strToolTip = "<b>" + strTitle + "</b>" + "<br /><br />" + strDescription  +  "<br /><br />" + strLink;

            QModelIndex index = model.index(i, 0);
            model.setData(index, strTitle, Qt::DisplayRole);
            model.setData(index, strToolTip, Qt::ToolTipRole);
            model.setData(index, strLink, Qt::UserRole);
        }
        return true;
    }
    return false;
}
