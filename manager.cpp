#include <QDomDocument>
#include <QDebug>
#include <QNetworkReply>
#include <QDesktopServices>
#include <QUrl>
#include <mycustomwindow.h>
#include <manager.h>

Manager::Manager(IMyCustomWindow *_view) : view(_view)
{
    connect(&netManager, &QNetworkAccessManager::finished, this, &Manager::managerFinished);
}

void Manager::downloadProgress(qint64 bytes, qint64 bytesTotal) const
{
    if (bytesTotal == -1 || bytesTotal == 0)
    {
        view->setProgressBar(0);
    }
    else
    {
        int percent = bytes * 100 / bytesTotal;
        view->setProgressBar(percent);
    }
}
void Manager::managerFinished(QNetworkReply * netReply)
{
    QString str (netReply->readAll());
    QDomDocument doc;
    if (!doc.setContent(str, false))
    {
        view->setStatusBMessage("Load Failed. Invalid RSS Feed");
    }
    else
    {
        if(model.updateModel(doc)) {
            view->setStatusBMessage("Load Finished. Double click item to open.");
        }
        else {
            view->setStatusBMessage("Load Finished. No item was found to display.");
        }
    }
}

bool Manager::openRSS(const QString &urlStr) {
    QUrl url(urlStr);
    if(url.isValid()) {
        view->setStatusBMessage("Loading " + urlStr);
        auto req = QNetworkRequest(url);
        req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
        connect(netManager.get(req), &QNetworkReply::downloadProgress,
                this, &Manager::downloadProgress);
        return true;
    }
    view->setStatusBMessage("Invalid URL");
    view->setProgressBar(0);
    return false;
}

void Manager::openItem(const QModelIndex &index) const
{
    auto strLink = model.getItemLink(index);
    if(!QDesktopServices::openUrl(QUrl(strLink)))
    {
        view->setStatusBMessage("Failed to open item: " + strLink);
    }
    else
    {
        view->setStatusBMessage("Opening item: " + strLink);
    }
}

QStandardItemModel* Manager::getModel()
{
    return model.getModel();
}
